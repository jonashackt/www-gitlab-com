---
layout: markdown_page
title: Checklist for becoming a Kubernetes Expert
---

Create an issue with this checklist on the [support team issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues/)
with the **Title:** *"Gitlab Kubernetes Boot Camp - your_name"*

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

```
**Goal of this checklist:** Set a clear path for Kubernetes Expert training

### Stage 1: Commit and Become familiar with what Kubernetes is

    1. [ ] Ping your manager on the issue to notify him you have started
    1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Kubernetes questions to you
    1. [ ] Watch [Managing Cloud Native Applications with Kubernetes - End to End] (https://www.youtube.com/watch?v=yOg7BxZLwUg&t=64s)
    1. [ ] Read [Kubernetes Concepts] (https://kubernetes.io/docs/concepts/)
    1. [ ] Follow Tutorial: [Kubernetes Basics] (https://kubernetes.io/docs/tutorials/kubernetes-basics/)
    1. [ ] Take Linux Foundation's [Introduction to Kubernetes] course (https://training.linuxfoundation.org/linux-courses/system-administration-training/introduction-to-kubernetes)
    1. [ ] Familirize yourself with [Kubernetes Operations (kops)] (https://github.com/kubernetes/kops)
    1. [ ] Familirize yourself with [Helm] (https://docs.helm.sh/)
    1. [ ] Google Cloud: Setup a Cluster using [Google Cloud Console & Cloud SDK] (https://cloud.google.com/kubernetes-engine/docs/how-to/creating-a-container-cluster), you have access to a Google Cloud project called `gitlab-demo` with your `@gitlab.com` Google Apps account.

### Stage 2: GitLab & Kubernetes

    1. [ ] [Install GitLab on a Kubernetes Cluster] (https://docs.gitlab.com/ee/install/kubernetes/)
    1. [ ] [Running GitLab Runner on a Kubernetes Cluster] (https://docs.gitlab.com/ee/install/kubernetes/gitlab_runner_chart)
    1. [ ] [Demo – CI/CD with GitLab in action] (/2017/03/13/ci-cd-demo/)
    1. [ ] [Idea to Production - with GitLab and Kubernetes] (https://www.itnotes.de/gitlab/kubernetes/k8s/gke/gcloud/2017/03/05/idea-to-production-with-gitlab-and-kubernetes/)
    1. [ ] [GitLab Helm Charts] (https://gitlab.com/charts/charts.gitlab.io)
    1. [ ] [GitLab Cloud Native Charts] (https://gitlab.com/charts/gitlab)


### Stage 3: Practical

- [ ] **Done with Stage 4**

Remember to contribute to any documentation that needs updating

    1. [ ] Deploy `gitlab-omnibus` on a Kubernetes Cluster
    1. [ ] Deploy a GitLab Runner on a Cluster


### Stage 4: Tickets

- [ ] **Done with Stage 5**

- [ ] Contribute valuable responses on at least 20 Kubernetes tickets, 10 of which should contain reproduceable issues. Even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay it to
the customer.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

#### Reproduceable Issues
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### [ ] Stage 5: Quiz?

### [ ] Stage 6: (Optional)
  - [ ] Take Linux Foundation's [Kubernetes Fundamentals course + certification] (https://training.linuxfoundation.org/linux-courses/system-administration-training/kubernetes-fundamentals) bundle (PAID)

### Final Stage

- [ ] Your Manager needs to check this box to acknowledge that you finished
- [ ] Send a MR to declare yourself a **Kubernetes Expert** on the team page
```
